// Create a standard server application using Node.JS.

// Get the http moduel using require() directive. Repackage the module on a new variable.
const http = require('http');

const host = 4000;
// Create the server and place it inside a new variable to give it an identifier.
let server = http.createServer((req, res) => {
	res.end('Welcome to the App!')
});

// Assign a designated port that will serve the project, by binding the connection with the desired port.
server.listen(host);

console.log(`Listening on port: ${host}`)